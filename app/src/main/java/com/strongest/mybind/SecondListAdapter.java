package com.strongest.mybind;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

/**
 * Created by zvihe on 01/11/2017.
 */

public class SecondListAdapter extends ArrayAdapter {

    Context context;

    SecondListAdapter(Context context, String[] s){
        super(context, R.layout.list_second, s);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView iv;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.list_second,null);
            iv = (ImageView) convertView.findViewById(R.id.list_second_iv);
            convertView.setTag(iv);
        }else{
            iv = (ImageView) convertView.getTag();
        }
        iv.setImageResource(R.drawable.camp);
        return convertView;
    }
}
