package com.strongest.mybind;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by zvihe on 01/11/2017.
 */

public class MainListAdapter extends ArrayAdapter {

    private Context context;
    private String[] s;


    MainListAdapter(Context context, String[] s){
        super(context, R.layout.list_main, s);
        this.context = context;
        this.s = s;
    }

    class ViewHolder{
        ImageView iv;
        TextView tv;
        CheckBox cb;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        if(convertView == null){
            vh = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate( R.layout.list_main, null);
            vh.iv = (ImageView) convertView.findViewById(R.id.list_main_iv);
            vh.tv = (TextView) convertView.findViewById(R.id.list_main_tv);
            vh.cb = (CheckBox) convertView.findViewById(R.id.list_main_cb);
            convertView.setTag(vh);
        }else{
            vh = (ViewHolder) convertView.getTag();
        }

        vh.iv.setImageResource(R.drawable.bus);
        vh.tv.setText(s[position]);
        vh.cb.setChecked(position%2 == 0);

        return convertView;
    }
}
